def bubble_sort(self, numbers):
    return sorted(numbers)

if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer number with space: "))).split()
    sorted_numbers = bubble_sort(numbers)
    print("Sorted number is", sorted_numbers)